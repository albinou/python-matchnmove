# Match & Move

This project aims to help me streaming Jack and Jill competitions.

The idea is to enter bibs numbers to a set of two keypads:

- one keypad for leaders
- another keypad for followers

When a bib is enter on a keypad, the script looks for the matching dancer's name
and write it to a text file that can be read by a video streaming program like
OBS Studio.

## Requirements

- python-evdev

## Installation

Coming soon ...

## Usage

Coming soon ...
