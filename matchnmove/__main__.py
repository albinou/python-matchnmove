#!/usr/bin/env python3

import argparse
import asyncio
import os
import sys

from .__about__ import (
    __author__,
    __email__,
    __description__,
    __url__,
    __license__,
    __version__,
)
from .dancers import Dancers
from .keypad import lookup_keypads


async def main():
    # parse args
    arg_parser = argparse.ArgumentParser(description=__description__)
    arg_parser.add_argument("-f",
                            "--csv-file",
                            type=argparse.FileType('r', encoding='utf-8'),
                            dest="csv_fd",
                            help="use this file to map inputs (CSV file)")
    arg_parser.add_argument("-d",
                            "--device",
                            action="append",
                            dest="devices_ids",
                            help="use devices with the specified vendor:product to read inputs")
    arg_parser.add_argument("-c",
                            "--current-dancers-file",
                            dest="current_dancers_filename",
                            default="/run/user/%s/matchnmove-dancers.txt" % os.getuid(),
                            help="use this file to retrieve the current dancers")
    arg_parser.add_argument("-l",
                            "--log-file",
                            type=argparse.FileType('a', encoding='utf-8'),
                            dest="log_fd",
                            help="use this file to log all entered dancers")
    arg_parser.add_argument("-v",
                            "--version",
                            action="version",
                            version="%s %s" % (__package__, __version__))
    args = arg_parser.parse_args()

    try:
        dancers = Dancers(args.csv_fd,
                          args.log_fd,
                          args.current_dancers_filename)
        args.csv_fd.close()
    except Exception as e:
        print(e)
        sys.exit(1)
    keypads = lookup_keypads(args.devices_ids)
    tasks = []
    for k in keypads:
        tasks.append(asyncio.create_task(
            k.handle_events(dancers.set_current_dancer)))
    for t in tasks:
        await t
    args.log_fd.close()
    return 0


if __name__ == '__main__':
    sys.exit(asyncio.run(main()))
