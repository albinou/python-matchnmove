import csv
import datetime


class Dancers:

    def __init__(self, csv_fd=None, log_fd=None,
                 current_dancers_filename=None):
        self._dancers = {}
        self._current_leader = None
        self._current_follower = None
        if csv_fd:
            self.load_csv(csv_fd)
        self._log_fd = log_fd
        self._current_dancers_filename = current_dancers_filename
        self.update_current_dancers()

    def load_csv(self, fd):
        reader = csv.DictReader(fd)
        for row in reader:
            bib = int(row['bib'])
            if bib in self._dancers.keys():
                raise Exception("Bib %d is duplicated in the input CSV file." %
                                bib)
            if bib >= 1000:
                raise Exception("Bib %d should be less than 1000." % bib)
            self._dancers[bib] = row['name']

    def _log(self, message):
        now = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        self._log_fd.write("%s: %s\n" % (now, message))
        self._log_fd.flush()

    def log_info(self, message):
        self._log("[INFO] %s" % message)

    def log_error(self, message):
        self._log("[ERROR] %s" % message)

    def unset_current_leader(self):
        self._current_leader = None
        self.update_current_dancers()
        return True

    def set_current_leader(self, bib):
        if bib not in self._dancers.keys():
            self.log_error("Invalid bib %s for the leader" % bib)
            print("Invalid bib %s for the leader" % bib)
            return False
        self._current_leader = bib
        self.log_info("New leader is: %s (bib = %s)" %
                      (self._dancers[bib], bib))
        self.update_current_dancers()
        return True

    def get_current_leader(self):
        return self._current_leader

    def get_current_leader_str(self):
        if self._current_leader:
            return self._dancers[self._current_leader]
        else:
            return None

    def unset_current_follower(self):
        self._current_follower = None
        self.update_current_dancers()
        return True

    def set_current_follower(self, bib):
        if bib not in self._dancers.keys():
            self.log_error("Invalid bib %s for the follower" % bib)
            print("Invalid bib %s for the follower" % bib)
            return False
        self._current_follower = bib
        self.log_info("New follower is: %s (bib = %s)" %
                      (self._dancers[bib], bib))
        self.update_current_dancers()
        return True

    def get_current_follower(self):
        return self._current_follower

    def get_current_follower_str(self):
        if self._current_follower:
            return self._dancers[self._current_follower]
        else:
            return None

    def set_current_dancer(self, role, bib):
        if role == "leader" or role == "leaders":
            if bib == 0:
                return self.unset_current_leader()
            else:
                return self.set_current_leader(bib)
        elif role == "follower" or role == "followers":
            if bib == 0:
                return self.unset_current_follower()
            else:
                return self.set_current_follower(bib)
        else:
            raise Exception("Role %s is unknown" % role)

    def update_current_dancers(self):
        txt = ""
        leader = self.get_current_leader_str()
        follower = self.get_current_follower_str()
        if leader and follower:
            txt = "%s & %s" % (leader, follower)
        elif leader:
            txt = leader
        elif follower:
            txt = follower
        if len(txt) == 0:
            self.log_info("Removing dancers")
        else:
            self.log_info("New dancers are: %s" % txt)
        with open(self._current_dancers_filename, 'w', encoding='utf-8') as fd:
            fd.truncate(0)
            fd.write(txt)
            fd.close()
