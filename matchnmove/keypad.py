import asyncio
from evdev import InputDevice, ecodes, events, list_devices


def lookup_keypads(devices_ids):
    ret = []
    devices = [InputDevice(path) for path in list_devices()]
    for d in devices:
        if "%0.4x:%0.4x" % (d.info.vendor, d.info.product) in devices_ids and \
           ecodes.KEY_0 in d.capabilities()[ecodes.EV_KEY]:
            ret.append(Keypad(d))
    return ret


class Keypad:

    def __init__(self, device):
        self._dev = device
        print("New device detected: %s" % self._dev)
        self._role = None
        self._led_numl_swtch_task = asyncio.create_task(
            self._led_numl_switch_loop())

    async def _led_numl_switch_loop(self):
        led_status = 1
        while self._role is None:
            led_status = int(not led_status)
            self._dev.set_led(ecodes.LED_NUML, led_status)
            await asyncio.sleep(0.25)

    def _bib_set(self):
        self._dev.set_led(ecodes.LED_NUML, 0)

    def _bib_unset(self):
        self._dev.set_led(ecodes.LED_NUML, 1)

    def _scancode_to_role(self, scancode):
        if scancode == ecodes.KEY_KP1:
            return "leaders"
        elif scancode == ecodes.KEY_KP2:
            return "followers"
        else:
            return None

    def _scancode_to_int(self, scancode):
        if scancode == ecodes.KEY_KP0:
            return 0
        elif scancode == ecodes.KEY_KP1:
            return 1
        elif scancode == ecodes.KEY_KP2:
            return 2
        elif scancode == ecodes.KEY_KP3:
            return 3
        elif scancode == ecodes.KEY_KP4:
            return 4
        elif scancode == ecodes.KEY_KP5:
            return 5
        elif scancode == ecodes.KEY_KP6:
            return 6
        elif scancode == ecodes.KEY_KP7:
            return 7
        elif scancode == ecodes.KEY_KP8:
            return 8
        elif scancode == ecodes.KEY_KP9:
            return 9
        else:
            return None

    async def handle_events(self, callback):
        nb_digits = 0
        bib = 0
        async for event in self._dev.async_read_loop():
            if event.type == ecodes.EV_KEY:
                k = events.KeyEvent(event)
                if k.keystate == k.key_down:
                    if self._role is None:
                        self._role = self._scancode_to_role(k.scancode)
                        if self._role:
                            print("Keypad %s has been registered for %s"
                                  % (self._dev.name, self._role))
                            self._bib_unset()
                    else:
                        digit = self._scancode_to_int(k.scancode)
                        if digit is not None:
                            nb_digits += 1
                            bib += digit * (10 ** (3 - nb_digits))
                            if nb_digits == 3:
                                if callback and not callback(self._role, bib):
                                    self._bib_unset()
                                else:
                                    self._bib_set()
                                bib = 0
                                nb_digits = 0
                        elif k.scancode == ecodes.KEY_KPDOT:
                            bib = 0
                            nb_digits = 0
                            if callback:
                                callback(self._role, 0)
                            self._bib_unset()
                        elif k.scancode == ecodes.KEY_BACKSPACE:
                            bib = 0
                            nb_digits = 0
